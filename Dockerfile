FROM webdevops/apache:latest

ARG LOCAL_DOMAIN=localhost
ENV LOCAL_DOMAIN $LOCAL_DOMAIN


COPY ./conf/vhost.conf /opt/docker/etc/httpd/vhost.conf
