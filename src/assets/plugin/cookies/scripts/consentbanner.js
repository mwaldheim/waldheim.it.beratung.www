var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Consentbanner = (function () {
    function Consentbanner(options) {
        if (options === void 0) { options = { delay: 0, debug: true, path: "/assets/plugin/cookies/config/", extension: "json" }; }
        this.check = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"check-square\" class=\"svg-inline--fa fa-check-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z\"></path></svg>";
        this.uncheck = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"times-square\" class=\"svg-inline--fa fa-times-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-54.4 289.1c4.7 4.7 4.7 12.3 0 17L306 377.6c-4.7 4.7-12.3 4.7-17 0L224 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L102.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L280 256l65.6 65.1z\"></path></svg>";
        this.json_buttons = {};
        this.json_cookies = {};
        this.json_details = {};
        this.json_dialog = {};
        this.json_links = {};
        this.json_types = {};
        this.json_types_aktiv = {};
        this.config = {
            delay: 0,
            debug: false,
            path: "/plugin/cookies/config/",
            extension: "json"
        };
        this.cookiesF = new cookie(false);
        this.settings = {};
        Object.assign(this.config, this.config, options);
        this.buildCheck();
    }
    Consentbanner.prototype.buildCheck = function () {
        return __awaiter(this, void 0, void 0, function () {
            var cookie, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.cookiesF.is_cookie_enabled()) return [3, 5];
                        return [4, this.loadConfig()];
                    case 1:
                        _a.sent();
                        return [4, this.loadSettings()];
                    case 2:
                        _a.sent();
                        cookie = this.cookiesF.get_cookie("COOKIE_CONFIRM_V3");
                        if (!cookie) return [3, 4];
                        return [4, this.loadSettings(true)];
                    case 3:
                        _a.sent();
                        this.save(true);
                        this.buildResetting();
                        if (this.config.debug) {
                            console.log("Resetting aufbauen");
                        }
                        return [3, 5];
                    case 4:
                        if (this.config.debug) {
                            console.log("Alert aufbauen");
                        }
                        url = window.location.href;
                        if (!url.includes(this.json_links.PRIVACY.URL) || !url.includes(this.json_links.IMPRINT.URL)) {
                            this.buildBanner();
                        }
                        _a.label = 5;
                    case 5: return [2];
                }
            });
        });
    };
    Consentbanner.prototype.loadSettings = function (blnOverload) {
        if (blnOverload === void 0) { blnOverload = false; }
        return __awaiter(this, void 0, void 0, function () {
            var json, cookieName, _i, _a, _b, key, value, Wrapper, _c, _d, key_1, value_1;
            return __generator(this, function (_e) {
                json = {};
                if (blnOverload) {
                    cookieName = this.json_cookies.NEEDED["COOKIE_CONFIRM"];
                    json = JSON.parse(this.cookiesF.get_cookie(cookieName.COOKIE_NAME));
                }
                else {
                    for (_i = 0, _a = Object.entries(this.json_cookies); _i < _a.length; _i++) {
                        _b = _a[_i], key = _b[0], value = _b[1];
                        Wrapper = key;
                        for (_c = 0, _d = Object.entries(value); _c < _d.length; _c++) {
                            key_1 = _d[_c];
                            value_1 = 0;
                            if (Wrapper === "NEEDED") {
                                value_1 = 1;
                            }
                            json[key_1[0]] = value_1;
                        }
                    }
                }
                this.settings = json;
                return [2];
            });
        });
    };
    Consentbanner.prototype.save = function (blockReload) {
        var _this = this;
        if (blockReload === void 0) { blockReload = false; }
        var Elements = document.querySelectorAll('[data-cookiecontens="accepted"]');
        Elements.forEach(function (ele) {
            var cookiename = ele.dataset.cookiename;
            if (_this.settings[cookiename] !== 1) {
                _this.runScript(ele);
            }
        });
        this.cookiesF.set_cookie('COOKIE_CONFIRM_V3', JSON.stringify(this.settings), 365);
        if (!blockReload) {
            window.location.reload();
        }
    };
    Consentbanner.prototype.saveAll = function () {
        var _this = this;
        var Elements = document.querySelectorAll('.swb_contens_entry');
        Elements.forEach(function (ele) {
            var cookiename = ele.dataset.cookie;
            if (typeof cookiename !== "undefined") {
                var icon = ele.querySelector('.icon');
                icon.classList.remove('unable');
                icon.classList.add('enable');
                icon.innerHTML = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"check-square\" class=\"svg-inline--fa fa-check-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z\"></path></svg>";
                _this.settings[cookiename] = 1;
            }
        });
        this.save();
    };
    Consentbanner.prototype.getParent = function (ele) {
        ele = ele.parentElement;
        return ele;
    };
    Consentbanner.prototype.change = function (event) {
        var target = event.target;
        while (!target.classList.contains('swb_contens_entry')) {
            target = this.getParent(target);
        }
        var cookie = target.dataset.cookie;
        var isChecked = this.settings[cookie];
        var icon = target.querySelector('.icon');
        if (isChecked === 1) {
            icon.classList.remove('enable');
            icon.classList.add('unable');
            icon.innerHTML = this.uncheck;
            this.settings[cookie] = 0;
        }
        else {
            icon.classList.remove('unable');
            icon.classList.add('enable');
            icon.innerHTML = this.check;
            this.settings[cookie] = 1;
        }
        console.log(this.settings);
    };
    Consentbanner.prototype.runScript = function (ele) {
        var tag = ele.tagName.toLowerCase();
        var isChange = false;
        switch (tag) {
            case "script":
                isChange = true;
                if (ele.getAttribute('type') === "text/javascript") {
                    ele.setAttribute('type', 'text/plain');
                }
                break;
            case "link":
                isChange = true;
                if (typeof ele.getAttribute('href') !== "undefined") {
                    ele.removeAttribute('href');
                }
                break;
            case "iframe":
                isChange = true;
                ele.dataset.src = ele.getAttribute('src');
                var link = ele.getAttribute('src');
                if (link.includes("youtube")) {
                    var vidID = link.substr(0, link.indexOf("?"));
                    if (vidID != "") {
                        vidID = vidID.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
                    }
                    else {
                        vidID = link.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
                    }
                    vidID += "/maxresdefault.jpg";
                    ele.style.backgroundImage = "url(" + vidID + ")";
                    ele.style.backgroundSize = "contain";
                }
                ele.setAttribute('src', "");
                break;
            case "button":
                isChange = true;
                ele.dataset.allow = 'false';
                break;
        }
        return isChange;
    };
    Consentbanner.prototype.buildResetting = function () {
        var _this = this;
        var setting_icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M352 328c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zM184 192c0-13.26-10.75-24-24-24s-24 10.74-24 24c0 13.25 10.75 24 24 24s24-10.75 24-24zm8 136c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zm96-96c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zm222.52 23.82c-69.97-.85-126.47-57.69-126.47-127.86-70.17 0-127-56.49-127.86-126.45C249.57.5 242.9 0 236.26 0c-20.68 0-41.18 4.85-59.79 14.33l-69.13 35.22a132.221 132.221 0 0 0-57.79 57.81l-35.1 68.88a132.645 132.645 0 0 0-12.82 80.95l12.08 76.28a132.555 132.555 0 0 0 37.16 72.96l54.77 54.76a132.036 132.036 0 0 0 72.71 37.06l76.71 12.14c6.86 1.09 13.76 1.62 20.64 1.62 20.72 0 41.25-4.88 59.89-14.38l69.13-35.22a132.221 132.221 0 0 0 57.79-57.81l35.1-68.88c12.56-24.63 17.01-52.57 12.91-79.9zm-41.42 65.36L434 390.07c-9.68 19-24.83 34.15-43.81 43.82l-69.13 35.22C307.08 476.23 291.39 480 275.7 480c-5.21 0-10.47-.41-15.63-1.23l-76.7-12.14c-21-3.33-40.05-13.04-55.09-28.08l-54.77-54.76c-15.1-15.09-24.84-34.23-28.18-55.33l-12.08-76.27c-3.35-21.12.02-42.36 9.72-61.41l35.1-68.88c9.68-19 24.83-34.15 43.81-43.82L191 42.85c11.33-5.77 23.8-9.33 36.51-10.46 13.15 63.15 63.84 112.95 127.25 124.86 11.91 63.42 61.71 114.11 124.87 127.25-1.1 12.73-4.64 25.14-10.53 36.68z\"/></svg>";
        var resettingButton = document.createElement("div");
        resettingButton.classList.add('resetting');
        resettingButton.id = 'swb_resetting';
        resettingButton.innerHTML = setting_icon;
        var deleteSettings = function () {
            _this.cookiesF.delete_cookie('COOKIE_CONFIRM_V3');
            _this.buildBanner();
            resettingButton.remove();
            if (_this.config.debug) {
                console.log('Cookies resettet');
            }
        };
        resettingButton.addEventListener('click', deleteSettings);
        var hoverEle = document.createElement("div");
        var hover = function () {
            hoverEle.id = "swb_resettinginfo";
            hoverEle.classList.add('resetting_infobox');
            hoverEle.innerHTML = _this.json_buttons.RESETTING;
            resettingButton.appendChild(hoverEle);
        };
        var deletehover = function () {
            hoverEle.remove();
        };
        resettingButton.addEventListener('mouseover', hover);
        resettingButton.addEventListener('mouseleave', deletehover);
        document.body.appendChild(resettingButton);
        setTimeout(function () {
            resettingButton.classList.add('loaded');
        }, 750);
    };
    Consentbanner.prototype.buildBanner = function () {
        var _this = this;
        var alert = document.createElement("div");
        alert.classList.add('swb_contens');
        alert.setAttribute('role', 'dialog');
        var title = document.createElement("div");
        title.classList.add('swb_contens_title');
        title.innerHTML = this.json_dialog.TITLE;
        var description = document.createElement("div");
        description.classList.add('swb_contens_description');
        description.innerHTML = this.json_dialog.DESCRIPTION;
        var privacy = document.createElement("a");
        privacy.innerHTML = this.json_links.PRIVACY.TITLE;
        privacy.setAttribute('alt', this.json_links.PRIVACY.HOVER);
        privacy.href = this.json_links.PRIVACY.URL;
        var imprint = document.createElement("a");
        imprint.innerHTML = this.json_links.IMPRINT.TITLE;
        imprint.setAttribute('alt', this.json_links.IMPRINT.HOVER);
        imprint.href = this.json_links.IMPRINT.URL;
        var links = document.createElement("div");
        links.classList.add('swb_contens_links');
        links.appendChild(privacy);
        links.appendChild(imprint);
        var saveButton = document.createElement('button');
        saveButton.innerHTML = this.json_buttons.SAVE;
        saveButton.classList.add('secoundary');
        saveButton.classList.add('outline');
        var save = function () {
            _this.save();
        };
        saveButton.addEventListener('click', save);
        var allButton = document.createElement('button');
        allButton.innerHTML = this.json_buttons.ALL;
        allButton.classList.add('success');
        var saveAll = function () {
            _this.saveAll();
        };
        allButton.addEventListener('click', saveAll);
        var buttons = document.createElement('div');
        buttons.classList.add('swb_contens_buttons');
        buttons.appendChild(saveButton);
        buttons.appendChild(allButton);
        var details_title = document.createElement('div');
        details_title.classList.add('swb_contens_details_title');
        details_title.innerHTML = this.json_details.TITLE;
        var details_description = document.createElement('div');
        details_description.classList.add('swb_contens_details_description');
        details_description.innerHTML = this.json_details.DESCRIPTION;
        var details_subtitle = document.createElement('div');
        details_subtitle.classList.add('swb_contens_details_subtitle');
        details_subtitle.innerHTML = this.json_details.SUBHEADER;
        var types = document.createElement('div');
        types.classList.add('swb_contens_types');
        alert.appendChild(title);
        alert.appendChild(description);
        alert.appendChild(details_title);
        alert.appendChild(details_description);
        for (var _i = 0, _a = Object.entries(this.json_types); _i < _a.length; _i++) {
            var _b = _a[_i], key = _b[0], value = _b[1];
            var wrapper = document.createElement('div');
            wrapper.id = key;
            wrapper.classList.add('swb_contens_entry_wrapper');
            var title_1 = document.createElement('div');
            title_1.classList.add('swb_contens_entry_title');
            if (typeof value === "string") {
                title_1.innerHTML = value;
            }
            wrapper.appendChild(title_1);
            var obj = this.json_cookies[key];
            if (typeof obj !== "undefined" && Object.keys(obj).length !== 0 && this.json_types_aktiv[key] === 1) {
                for (var _c = 0, _d = Object.entries(obj); _c < _d.length; _c++) {
                    var value_2 = _d[_c];
                    var cookie = value_2[1];
                    var entry = document.createElement('div');
                    entry.classList.add('swb_contens_entry');
                    var icon = document.createElement('div');
                    icon.classList.add('icon');
                    if (key === "NEEDED" || this.settings[cookie.COOKIE_NAME] === 1) {
                        icon.innerHTML = this.check;
                        icon.classList.add("enable");
                        icon.classList.add("needed");
                    }
                    else {
                        icon.innerHTML = this.uncheck;
                        icon.classList.add("unable");
                    }
                    var text = document.createElement('div');
                    text.classList.add('text');
                    var addOn = "";
                    if (cookie.TTL !== 0) {
                        addOn = " (" + cookie.TTL + " " + this.json_details.DAYS + ")";
                    }
                    text.innerHTML = cookie.DESCRIPTION + addOn;
                    if (key !== "NEEDED") {
                        var change = function (e) {
                            _this.change(e);
                        };
                        icon.addEventListener('click', change);
                        text.addEventListener('click', change);
                    }
                    entry.appendChild(icon);
                    entry.appendChild(text);
                    entry.dataset.cookie = cookie.COOKIE_NAME;
                    entry.dataset.cookiegroup = key;
                    wrapper.appendChild(entry);
                }
            }
            if (this.json_types_aktiv[key] === 1) {
                alert.appendChild(wrapper);
            }
        }
        alert.appendChild(buttons);
        alert.appendChild(links);
        document.body.appendChild(alert);
        setTimeout(function () {
            alert.classList.add('loaded');
        }, 750);
    };
    Consentbanner.prototype.loadConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var lang, arr, result, path, file, fileExist, response, json;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        lang = document.querySelector('html').getAttribute("lang");
                        if (typeof lang === "undefined") {
                            lang = "de";
                        }
                        else {
                            if (this.config.debug) {
                                console.log("Language detected: " + lang);
                            }
                        }
                        lang = "_" + lang;
                        arr = window.location.href.split("/");
                        result = arr[0] + "//" + arr[2];
                        path = result + this.config.path;
                        file = path + "cookies" + lang + "." + this.config.extension;
                        return [4, Consentbanner.doesFileExist(file)];
                    case 1:
                        fileExist = _a.sent();
                        if (!fileExist) {
                            lang = "_de";
                            file = path + "cookies" + lang + "." + this.config.extension;
                        }
                        return [4, fetch(file)];
                    case 2:
                        response = _a.sent();
                        return [4, response.json()];
                    case 3:
                        json = _a.sent();
                        return [2, new Promise(function (resolve) {
                                _this.splitJSON(json);
                                resolve(true);
                            })];
                }
            });
        });
    };
    Consentbanner.prototype.splitJSON = function (json) {
        this.json_buttons = json.BUTTONS;
        this.json_cookies = json.COOKIES;
        this.json_details = json.DETAILS;
        this.json_dialog = json.DIALOG;
        this.json_links = json.LINKS;
        this.json_types = json.TYPES;
        this.json_types_aktiv = json.TYPES_AKTIV;
    };
    Consentbanner.doesFileExist = function (urlToFile) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, fetch(urlToFile)];
                    case 1:
                        response = _a.sent();
                        return [2, response.status !== 404];
                }
            });
        });
    };
    return Consentbanner;
}());
//# sourceMappingURL=consentbanner.js.map