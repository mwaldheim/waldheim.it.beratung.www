/**
 * ConsentBanner
 * @author Markus Waldheim - Waldheim.dev <kontkat@waldheim.dev>
 * @version 3.0.0
 *
 */
class Consentbanner {
    check = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"check-square\" class=\"svg-inline--fa fa-check-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z\"></path></svg>";
    uncheck = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"times-square\" class=\"svg-inline--fa fa-times-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-54.4 289.1c4.7 4.7 4.7 12.3 0 17L306 377.6c-4.7 4.7-12.3 4.7-17 0L224 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L102.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L280 256l65.6 65.1z\"></path></svg>";


    // JSON-Loadings
    json_buttons: any = {};
    json_cookies: any = {};
    json_details: any = {};
    json_dialog: any = {};
    json_links: any = {};
    json_types: any = {};
    json_types_aktiv: any = {};

    // Configs
    config = {
        delay: 0,
        debug: false,
        path: "/plugin/cookies/config/",
        extension: "json"
    }

    // Cookie-Class
    // @ts-ignore
    cookiesF = new cookie(false);

    settings: {} = {}

    /**
     * Function start Application
     * @param options | {delay: 0,debug: false,path: "/plugin/cookies/config/",extension:"json"}
     */
    constructor(options: any = {delay: 0, debug: true, path: "/assets/plugin/cookies/config/",extension:"json"}) {
        // @ts-ignore
        Object.assign(this.config,this.config,options);
        this.buildCheck();
    }

    /**
     * Function Check for BuildProcesses
     * @private
     */
    private async buildCheck() {
        if (this.cookiesF.is_cookie_enabled()) {
            await this.loadConfig();
            await this.loadSettings();
            const cookie = this.cookiesF.get_cookie("COOKIE_CONFIRM_V3");
            if (cookie) {
                await this.loadSettings(true);
                this.save(true);
                this.buildResetting();
                if (this.config.debug) {
                    console.log("Resetting aufbauen");
                }
            } else {
                if (this.config.debug) {
                    console.log("Alert aufbauen")
                }
                const url:string = window.location.href;
                if (!url.includes(this.json_links.PRIVACY.URL) || !url.includes(this.json_links.IMPRINT.URL)) {
                    this.buildBanner();
                }
            }
        }
    }

    /**
     * Function try to load Element from Cookie or from Config
     * @param blnOverload
     * @private
     */
    private async loadSettings(blnOverload: boolean = false) {
        let json: any = {};
        if (blnOverload) {
            const cookieName: ContensCookie = this.json_cookies.NEEDED["COOKIE_CONFIRM"];
            json = JSON.parse(this.cookiesF.get_cookie(cookieName.COOKIE_NAME));
        } else {
            for (const [key, value] of Object.entries(this.json_cookies)) {
                const Wrapper = key;
                // key = wrapper
                for (const key of Object.entries(value)) {
                    let value = 0;
                    if (Wrapper === "NEEDED") {
                        value = 1;
                    }
                    // @ts-ignore
                    json[key[0]] = value
                }
            }
        }
        this.settings = json;
    }

    /**
     * Function save the Cookie-Settings like it clickt and reload the Page, if not blocked
     * @param blockReload
     * @private
     */
    private save(blockReload: boolean = false) {
        const Elements = document.querySelectorAll('[data-cookiecontens="accepted"]');
        Elements.forEach((ele: HTMLElement) => {
            const cookiename = ele.dataset.cookiename;
            // @ts-ignore
            if (this.settings[cookiename] !== 1) {
                this.runScript(ele);
            }
        });
        this.cookiesF.set_cookie('COOKIE_CONFIRM_V3', JSON.stringify(this.settings), 365);
        if (!blockReload) {
            window.location.reload();
        }
    }

    /**
     * Function activate all Cookies and start the Save-Call
     * @private
     */
    private saveAll() {
        const Elements = document.querySelectorAll('.swb_contens_entry');
        Elements.forEach((ele: HTMLElement) => {
            const cookiename = ele.dataset.cookie;
            if (typeof cookiename !== "undefined") {
                let icon: HTMLElement = ele.querySelector('.icon');
                icon.classList.remove('unable');
                icon.classList.add('enable');
                icon.innerHTML = "<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"check-square\" class=\"svg-inline--fa fa-check-square fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z\"></path></svg>";
                // @ts-ignore
                this.settings[cookiename] = 1;
            }
        });
        this.save();
    }

    /**
     * Funktion tauscht Element durch Parent aus.
     * @param ele
     * @private
     */
    private getParent(ele: HTMLElement) {
        ele = ele.parentElement;
        return ele;
    }

    /**
     * Function Change one single Option, when it's clicked
     * @param event
     * @private
     */
    private change(event: KeyboardEvent) {
        // @ts-ignore
        let target: HTMLElement = event.target;
        while(!target.classList.contains('swb_contens_entry')) {
            target = this.getParent(target);
        }
        const cookie = target.dataset.cookie;
        // @ts-ignore
        const isChecked = this.settings[cookie];
        let icon: HTMLElement = target.querySelector('.icon');
        if (isChecked === 1) {
            icon.classList.remove('enable');
            icon.classList.add('unable');
            icon.innerHTML = this.uncheck;
            // @ts-ignore
            this.settings[cookie] = 0;
        } else {
            icon.classList.remove('unable');
            icon.classList.add('enable');
            icon.innerHTML = this.check;
            // @ts-ignore
            this.settings[cookie] = 1;
        }
        console.log(this.settings);
    }

    /**
     * Function run Replaces to activate the Third-Part Scripts
     * @param ele
     * @private
     */
    private runScript(ele: HTMLElement) {
        const tag = ele.tagName.toLowerCase();
        let isChange = false;
        switch (tag) {
            case "script":
                isChange = true;
                if (ele.getAttribute('type') === "text/javascript") {
                    ele.setAttribute('type', 'text/plain');
                }
                break;
            case "link":
                isChange = true;
                if (typeof ele.getAttribute('href') !== "undefined") {
                    ele.removeAttribute('href');
                }
                break;
            case "iframe":
                isChange = true;
                ele.dataset.src = ele.getAttribute('src');
                const link = ele.getAttribute('src');
                if (link.includes("youtube")) {
                    let vidID = link.substr(0, link.indexOf("?"));
                    if(vidID != ""){
                        vidID = vidID.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
                    }else{
                        vidID = link.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
                    }
                    vidID += "/maxresdefault.jpg";
                    ele.style.backgroundImage = "url(" + vidID + ")";
                    ele.style.backgroundSize = "contain";
                }
                ele.setAttribute('src', "");
                break;
            case "button":
                isChange = true;
                ele.dataset.allow = 'false';
                break;
        }
        return isChange;
    }

    /**
     * Function build Resetting-Drop to reset configs and load old config
     * @private
     */
    private buildResetting() {
        const setting_icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M352 328c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zM184 192c0-13.26-10.75-24-24-24s-24 10.74-24 24c0 13.25 10.75 24 24 24s24-10.75 24-24zm8 136c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zm96-96c-13.25 0-24 10.74-24 24 0 13.25 10.75 24 24 24s24-10.75 24-24c0-13.26-10.75-24-24-24zm222.52 23.82c-69.97-.85-126.47-57.69-126.47-127.86-70.17 0-127-56.49-127.86-126.45C249.57.5 242.9 0 236.26 0c-20.68 0-41.18 4.85-59.79 14.33l-69.13 35.22a132.221 132.221 0 0 0-57.79 57.81l-35.1 68.88a132.645 132.645 0 0 0-12.82 80.95l12.08 76.28a132.555 132.555 0 0 0 37.16 72.96l54.77 54.76a132.036 132.036 0 0 0 72.71 37.06l76.71 12.14c6.86 1.09 13.76 1.62 20.64 1.62 20.72 0 41.25-4.88 59.89-14.38l69.13-35.22a132.221 132.221 0 0 0 57.79-57.81l35.1-68.88c12.56-24.63 17.01-52.57 12.91-79.9zm-41.42 65.36L434 390.07c-9.68 19-24.83 34.15-43.81 43.82l-69.13 35.22C307.08 476.23 291.39 480 275.7 480c-5.21 0-10.47-.41-15.63-1.23l-76.7-12.14c-21-3.33-40.05-13.04-55.09-28.08l-54.77-54.76c-15.1-15.09-24.84-34.23-28.18-55.33l-12.08-76.27c-3.35-21.12.02-42.36 9.72-61.41l35.1-68.88c9.68-19 24.83-34.15 43.81-43.82L191 42.85c11.33-5.77 23.8-9.33 36.51-10.46 13.15 63.15 63.84 112.95 127.25 124.86 11.91 63.42 61.71 114.11 124.87 127.25-1.1 12.73-4.64 25.14-10.53 36.68z\"/></svg>";
        let resettingButton = document.createElement("div");
        resettingButton.classList.add('resetting');
        resettingButton.id = 'swb_resetting';
        resettingButton.innerHTML = setting_icon;
        let deleteSettings = () => {
            this.cookiesF.delete_cookie('COOKIE_CONFIRM_V3');
            this.buildBanner();
            resettingButton.remove();
            if (this.config.debug) {
                console.log('Cookies resettet');
            }
        };
        resettingButton.addEventListener('click', deleteSettings);
        let hoverEle = document.createElement("div");
        let hover = () => {
            hoverEle.id = "swb_resettinginfo";
            hoverEle.classList.add('resetting_infobox');
            hoverEle.innerHTML = this.json_buttons.RESETTING;
            resettingButton.appendChild(hoverEle);
        }
        let deletehover = () => {
            hoverEle.remove();
        }
        resettingButton.addEventListener('mouseover', hover);
        resettingButton.addEventListener('mouseleave', deletehover);
        document.body.appendChild(resettingButton);
        setTimeout(() => {
            resettingButton.classList.add('loaded')
        }, 750);
    }

    /**
     * Function bild ConsentBanner with all Functioncalls
     * @private
     */
    private buildBanner() {

        //<editor-fold desc="Intro-Dialog">
        let alert = document.createElement("div");
        alert.classList.add('swb_contens');
        alert.setAttribute('role', 'dialog');

        let title = document.createElement("div");
        title.classList.add('swb_contens_title');
        title.innerHTML = this.json_dialog.TITLE;

        let description = document.createElement("div");
        description.classList.add('swb_contens_description');
        description.innerHTML = this.json_dialog.DESCRIPTION;

        let privacy = document.createElement("a");
        privacy.innerHTML = this.json_links.PRIVACY.TITLE;
        privacy.setAttribute('alt', this.json_links.PRIVACY.HOVER);
        privacy.href = this.json_links.PRIVACY.URL;

        let imprint = document.createElement("a");
        imprint.innerHTML = this.json_links.IMPRINT.TITLE;
        imprint.setAttribute('alt', this.json_links.IMPRINT.HOVER);
        imprint.href = this.json_links.IMPRINT.URL;

        let links = document.createElement("div");
        links.classList.add('swb_contens_links');
        links.appendChild(privacy);
        links.appendChild(imprint);
        //</editor-fold>

        //<editor-fold desc="Buttons">
        let saveButton = document.createElement('button');
        saveButton.innerHTML = this.json_buttons.SAVE;
        saveButton.classList.add('secoundary');
        saveButton.classList.add('outline');
        // @ts-ignore
        let save = () => {
            this.save();
        }
        saveButton.addEventListener('click', save);

        let allButton = document.createElement('button');
        allButton.innerHTML = this.json_buttons.ALL;
        allButton.classList.add('success');
        let saveAll = () => {
            this.saveAll();
        }
        allButton.addEventListener('click', saveAll);

        let buttons = document.createElement('div');
        buttons.classList.add('swb_contens_buttons');
        buttons.appendChild(saveButton);
        buttons.appendChild(allButton);
        //</editor-fold>

        //<editor-fold desc="Footer">
        let details_title = document.createElement('div');
        details_title.classList.add('swb_contens_details_title');
        details_title.innerHTML = this.json_details.TITLE;

        let details_description = document.createElement('div');
        details_description.classList.add('swb_contens_details_description');
        details_description.innerHTML = this.json_details.DESCRIPTION;

        let details_subtitle = document.createElement('div');
        details_subtitle.classList.add('swb_contens_details_subtitle');
        details_subtitle.innerHTML = this.json_details.SUBHEADER;

        let types = document.createElement('div');
        types.classList.add('swb_contens_types');
        //</editor-fold>

        //<editor-fold desc="Appends">
        alert.appendChild(title);
        alert.appendChild(description);
        alert.appendChild(details_title);
        alert.appendChild(details_description);
        //</editor-fold>


        //<editor-fold desc="Entry-Builds">
        for (const [key, value] of Object.entries(this.json_types)) {
            let wrapper = document.createElement('div');
            wrapper.id = key;
            wrapper.classList.add('swb_contens_entry_wrapper');
            let title = document.createElement('div');
            title.classList.add('swb_contens_entry_title');
            if (typeof value === "string") {
                title.innerHTML = value;
            }
            wrapper.appendChild(title);
            let obj = this.json_cookies[key];
            if (typeof obj !== "undefined" && Object.keys(obj).length !== 0 && this.json_types_aktiv[key] === 1) {
                for (const value of Object.entries(obj)) {
                    let cookie = value[1];
                    let entry = document.createElement('div');
                    entry.classList.add('swb_contens_entry');
                    let icon = document.createElement('div');
                    icon.classList.add('icon');
                    // @ts-ignore
                    if (key === "NEEDED" || this.settings[cookie.COOKIE_NAME] === 1) {
                        icon.innerHTML = this.check;
                        icon.classList.add("enable");
                        icon.classList.add("needed");
                    } else {
                        icon.innerHTML = this.uncheck;
                        icon.classList.add("unable");
                    }
                    let text = document.createElement('div');
                    text.classList.add('text');
                    let addOn = "";
                    // @ts-ignore
                    if (cookie.TTL !== 0) {
                        // @ts-ignore
                        addOn = " (" + cookie.TTL + " " + this.json_details.DAYS + ")";
                    }
                    // @ts-ignore
                    text.innerHTML = cookie.DESCRIPTION + addOn;

                    if (key !== "NEEDED") {
                        let change = (e: KeyboardEvent) => {
                            this.change(e);
                        }
                        icon.addEventListener('click', change);
                        text.addEventListener('click', change);
                    }

                    entry.appendChild(icon);
                    entry.appendChild(text);

                    // @ts-ignore
                    entry.dataset.cookie = cookie.COOKIE_NAME;
                    entry.dataset.cookiegroup = key;

                    wrapper.appendChild(entry);
                }
            }
            if (this.json_types_aktiv[key] === 1) {
                alert.appendChild(wrapper);
            }
        }
        //</editor-fold>

        alert.appendChild(buttons);
        alert.appendChild(links);
        document.body.appendChild(alert);

        setTimeout(() => {
            alert.classList.add('loaded')
        }, 750);
    }


    /**
     * Function load File and split it into the Elements
     * @private
     */
    private async loadConfig(): Promise<boolean> {
        let lang = document.querySelector('html').getAttribute("lang");
        if (typeof lang === "undefined") {
            lang = "de"
        } else {
            if (this.config.debug) {
                console.log("Language detected: " + lang);
            }
        }
        lang = "_" + lang;
        const arr = window.location.href.split("/");
        const result = arr[0] + "//" + arr[2];
        const path = result + this.config.path;
        let file = path + "cookies" + lang + "."+this.config.extension;
        const fileExist = await Consentbanner.doesFileExist(file);
        if (!fileExist) {
            lang = "_de";
            file = path + "cookies" + lang + "."+this.config.extension;
        }
        let response = await fetch(file);
        const json = await response.json();
        return new Promise(resolve => {
            this.splitJSON(json);
            resolve(true);
        });
    }

    /**
     * Function split JSON to variables
     * @param json
     * @private
     */
    private splitJSON(json: ContensJSON) {
        this.json_buttons = json.BUTTONS;
        this.json_cookies = json.COOKIES;
        this.json_details = json.DETAILS;
        this.json_dialog = json.DIALOG;
        this.json_links = json.LINKS;
        this.json_types = json.TYPES;
        this.json_types_aktiv = json.TYPES_AKTIV;
    }

    /**
     * Function check if file exist
     * @param urlToFile
     * @private
     */
    private static async doesFileExist(urlToFile: string): Promise<boolean> {
        let response = await fetch(urlToFile);
        return response.status !== 404;
    }

    // Funktionen


}

interface ContensJSON {
    BUTTONS: any,
    COOKIES: any,
    DETAILS: any,
    DIALOG: any,
    LINKS: any,
    TYPES: any,
    TYPES_AKTIV: any
}

interface ContensCookie {
    "COOKIE_NAME": string,
    "DESCRIPTION": string,
    "TTL": number
}
