var cookie = (function () {
    function cookie(debug) {
        if (debug === void 0) { debug = false; }
        this.debug_mode = debug;
    }
    cookie.prototype.get_cookie = function (cookieName) {
        if (this.debug_mode)
            console.log(cookieName);
        var strValue = undefined;
        if (this.strCookie = document.cookie) {
            if (this.arrCookie = this.strCookie.match(new RegExp(cookieName + '=([^;]*)', 'g'))) {
                strValue = RegExp.$1;
            }
        }
        if (this.debug_mode)
            console.log(strValue);
        return (strValue);
    };
    cookie.prototype.set_cookie = function (cookieName, cookieValue, intDays) {
        if (!this.is_cookie_enabled()) {
            return false;
        }
        var objNow = new Date();
        var strExp = new Date(objNow.getTime() + (intDays * 86400000));
        document.cookie = cookieName + '=' +
            cookieValue + ';expires=' +
            strExp.toUTCString() + ';path=/;';
        if (this.debug_mode)
            console.log(document.cookie);
        return true;
    };
    cookie.prototype.delete_cookie = function (cookieName) {
        if (this.debug_mode)
            console.log(cookieName);
        if (document.cookie) {
            this.set_cookie(cookieName, "", -1);
            return true;
        }
        return false;
    };
    cookie.prototype.is_cookie_enabled = function () {
        if (typeof navigator.cookieEnabled !== 'undefined') {
            return navigator.cookieEnabled;
        }
        this.set_cookie('testcookie', 'testwert', 1);
        if (!document.cookie) {
            return false;
        }
        this.delete_cookie('testcookie');
        if (this.debug_mode)
            console.log("Cookies active");
        return true;
    };
    return cookie;
}());
var cookies = new cookie(false);
//# sourceMappingURL=cookie.js.map