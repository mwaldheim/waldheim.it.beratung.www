/**
 * Klasse für Cookies
 */
class cookie {
    debug_mode: boolean;
    strCookie: string;
    arrCookie: string[];

    constructor(debug = false) {
        this.debug_mode = debug;
    }


    /**
     *
     * Funktion holt einen bestimmten Cookie, welcher im Übergabeparameter abgeholt wird.
     *
     * @author Markus Waldheim
     * @since 2017-11-16

     * @param cookieName
     */
    get_cookie(cookieName: string) {
        if (this.debug_mode) console.log(cookieName);
        let strValue: any = undefined;
        if (this.strCookie = document.cookie) {
            if (this.arrCookie = this.strCookie.match(new RegExp(cookieName + '=([^;]*)', 'g'))) {
                strValue = RegExp.$1;
            }
        }
        if (this.debug_mode) console.log(strValue);
        return (strValue);
    }

    /**
     * Funktion setzt Cookie mit bestimmten Wert
     * @param cookieName
     * @param cookieValue
     * @param intDays
     */
    set_cookie(cookieName: string, cookieValue: any, intDays: number) {
        if (!this.is_cookie_enabled()) {
            return false;
        }
        var objNow = new Date();
        var strExp = new Date(objNow.getTime() + (intDays * 86400000));
        document.cookie = cookieName + '=' +
            cookieValue + ';expires=' +
            strExp.toUTCString() + ';path=/;';
        if (this.debug_mode) console.log(document.cookie);
        return true;
    }

    delete_cookie(cookieName: string) {
        if (this.debug_mode) console.log(cookieName);
        if (document.cookie) {
            this.set_cookie(cookieName, "", -1);
            return true;
        }
        return false;
    }

    is_cookie_enabled() {
        if (typeof navigator.cookieEnabled !== 'undefined') {
            return navigator.cookieEnabled;
        }
        this.set_cookie('testcookie', 'testwert', 1);
        if (!document.cookie) {
            return false;
        }
        this.delete_cookie('testcookie');
        if (this.debug_mode) console.log("Cookies active");
        return true;
    }
}

// Allgemein abspeichern
const cookies: cookie = new cookie(false);
